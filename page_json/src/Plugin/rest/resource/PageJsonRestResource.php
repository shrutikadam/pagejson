<?php

namespace Drupal\page_json\Plugin\rest\resource;


use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\node\Entity\Node;


/**
 * @RestResource(
 *   id = "page_json_resource",
 *   label = @Translation("Json representation of page node"),
 *   uri_paths = {
 *     "canonical" = "/page_json/{setkey}/{nid}"
 *   }
 * )
 */
class PageJsonRestResource extends ResourceBase {

  /**
   * @param $api_key to match with siteapikey
   * @param $nid
   * Get MEthod to fetch json representation of a given nid if it is a page and
   *   siteapikey matches
   *
   * @return \Drupal\rest\ResourceResponse
   */
  public function get($api_key, $nid) {
    // Get stored value
    $api_get = \Drupal::config('site_key_form.settings')
      ->get('siteapikey');
    // load node of a given $nid
    $node_details = Node::load($nid);
    // Condition to check if $nid exists and it is of type page and both the keys are matching
    if (!empty($node_details) && ($node_details->getType() == 'page') && ($api_get == $api_key)) {
      return new ResourceResponse($node_details);
    }
    // return access denied
    else {
      return new ResourceResponse('Access denied', 403);
    }
  }

}
